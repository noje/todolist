console.log('- Début Todolist');

let item1 = {
    texte: "revoir cours",
    completed: false
}

let item2 = {
    texte: "new file",
    completed: true
}

let item3 = {
    texte: "refaire todolist",
    completed: false
}

let todoList = {
    todos: [item1,item2,item3],
    titre: "Ma todo",
    displayTodos: function() {
        console.log(this.titre, " :");
        this.todos.forEach(function(todo){
            if (todo.completed) {
                console.log("(x)",todo.texte);
            } else {
                console.log("( )",todo.texte);
            }
        });
    },
    addTodo: function(newtodo) {
        this.todos.push({texte: newtodo, completed: false});
        this.displayTodos();
    },
    deleteTodo: function(index) {
        this.todos.splice(index,1);
        this.displayTodos();
    },
    changeTodo: function(index,newtodo) {
        this.todos[index] =  {
            texte: newtodo,
            completed: this.todos[index].completed
        };
        this.displayTodos();
    },
    toggleCompleted: function(index) {
        this.todos[index].completed = !this.todos[index].completed;
        this.displayTodos();
    },
    toggleAll: function(){
        let totalTodos = this.todos.length;
        let completedTodos = 0;
        
        // calculer le nombre de vrai dans le tableau
        this.todos.forEach(function (item) {
            if (item.completed) {
                completedTodos++;
            }
        });
        this.todos.forEach(function(todo){
                if (totalTodos === completedTodos) {
                    todo.completed = false;
                } else {
                    todo.completed = true;
                }
            });
    
            todoList.displayTodos();
        }
    }

    let handlers = {
        displayTodos: function() {
            todoList.displayTodos();
        },
        toggleAll: function() {
            todoList.toggleAll();
            view.displayTodos();
        },
        addTodo: function() {
            let addTodoTextInputElement = document.getElementById('addTodoTextInput');
            todoList.addTodo(addTodoTextInputElement.value);
            addTodoTextInputElement.value = '';
            view.displayTodos();
        },
        deleteTodo: function(position) {
            todoList.deleteTodo(position);
            view.displayTodos();
        },
        toggleCompleted: function(position) {
            todoList.toggleCompleted(position);
            view.displayTodos();
        },
        changeTodo: function() {
            let changeTodoTextInputElement = document.getElementById('changeTodoTextInput');
            let changeTodoPositionInputElement = document.getElementById('changeTodoPositionInput');
            todoList.changeTodo(changeTodoPositionInputElement.value,changeTodoTextInputElement.value);
            changeTodoTextInputElement.value = '';
            changeTodoPositionInputElement.value = '';
            view.displayTodos();
        }
    }


    let view = {
        displayTodos: function () {
            let todosUl = document.querySelector('ul');
            todosUl.innerHTML = '';

            todoList.todos.forEach(function(todo,index){ 
                let todoLi = document.createElement('li');
                
                todoLi.id = index;
                
                todoLi.appendChild(this.createCheckbox(todo.completed));
                todoLi.appendChild(document.createTextNode(todo.texte));
                todoLi.appendChild(this.createDeleteButton());
                
                todosUl.appendChild(todoLi);
                      
        }, this);
    },
    createDeleteButton: function(){
        let deleteButton = document.createElement('button');
        deleteButton.textContent = 'Supprimer';
        deleteButton.className = 'deleteButton';
        return deleteButton;
    },
    createCheckbox: function(checked){
        let doneCheckbox = document.createElement('input');
        doneCheckbox.type = "checkbox";
        doneCheckbox.checked = checked;
        doneCheckbox.className = 'doneCheckbox';
        return doneCheckbox;
    },
    setUpEventListeners: function(){
        let todosUl = document.querySelector('ul');
        todosUl.addEventListener('click', function(event){
            // console.log(event);
            // console.log(event.target.parentNode.id);
            // Trouver l'élément cliquer
            let elementClicked = event.target;
            // Si mon élément cliqué est un bouton deleteButton
            if (elementClicked.className === 'deleteButton'){
            // supprimer l'élément
            handlers.deleteTodo(parseInt(elementClicked.parentNode.id));
            }
            // Si mon élément cliqué est une checkbox
            if (elementClicked.className === 'doneCheckbox'){
            // toggle l'élement
            handlers.toggleCompleted(parseInt(elementClicked.parentNode.id));
            }
            
        });
    }
}
todoList.displayTodos();
view.displayTodos();
view.setUpEventListeners();
